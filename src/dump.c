#include <linux/smp.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/binfmts.h>
#include <asm/mman.h>
#include <asm/uaccess.h>
#include <linux/file.h>
#include <linux/sys.h>
#include <asm/page.h>
#include "config.h"
#include "ckpt.h"
#include "ckptlib.h"

#include <linux/mount.h>
#include <linux/swapops.h>
#include <linux/sched.h>



/*
 * Write a packet of data to a file.
 */
int pack_write(struct file *f, char *buf, 
	       int len, int last_pkt, int flag) 
{
	static char *pack = NULL;
	static long pos = 0;
	int ret, to_copy, wrtn = 0;
	
	if (!pack) {
		if (!(pack=(char*)kmalloc(PACKET_SIZE, GFP_KERNEL)))
			return -1;
	}
		
	while (len>0) {
		to_copy = (len>(PACKET_SIZE-pos))?(PACKET_SIZE-pos):(len);
		
		if (flag==FROM_USER)
			copy_from_user(&(pack[pos]), buf+wrtn, to_copy);
		else
			ckpt_strncpy(&(pack[pos]), buf+wrtn, to_copy);
		
		pos += to_copy;
		len -= to_copy;
		wrtn +=to_copy; 
		
		// If we've reached the last data.
		if ( (pos==PACKET_SIZE) || (last_pkt) )	{
			mm_segment_t fs = get_fs();
			
			set_fs(KERNEL_DS);
			ret = f->f_op->write(f, pack, pos, &(f->f_pos));	
			set_fs(fs);
			if (ret!=pos)
				return ret;
			
			pos = 0;
			if (last_pkt) {
				kfree(pack);
				pack = NULL;
			}
		}
	}
	
	if ( (last_pkt) && (pack!=NULL) ) {
		if (pos!=0) {
			mm_segment_t fs = get_fs();
			
			set_fs(KERNEL_DS);
			wrtn = f->f_op->write(f, pack, pos, &f->f_pos);
			set_fs(fs);
		}
		kfree(pack);
		pack = NULL;
		pos = 0;
	}	
	
	return wrtn;	
}



void dump_header_struct(struct file *file, struct task_struct *p, int in_sys_call)
{
	struct header hdr;
	
	ckpt_strncpy(hdr.signature, "CKPT", 4);
	hdr.major_version = CKPT_MAJOR;
	hdr.minor_version = CKPT_MINOR;	
	hdr.num_segments  = p->mm->map_count;
	
	task_lock(p);
	hdr.pid         = p->pid;
	hdr.uid         = p->uid;
	hdr.euid        = p->euid;
	hdr.suid        = p->suid;
	hdr.fsuid       = p->fsuid;
	hdr.gid         = p->gid;
	hdr.egid        = p->egid;
	hdr.sgid        = p->sgid;
	hdr.fsgid       = p->fsgid;
	hdr.ngroups     = p->group_info->ngroups;
	hdr.in_sys_call = in_sys_call;
	ckpt_strncpy(hdr.comm, p->comm, 16);
	task_unlock(p);
	
	pack_write(file, (void*)&hdr, sizeof(struct header), 0, FROM_KERNEL);
}

void dump_memory_struct(struct file *file, struct task_struct *p)
{
	struct mm_struct *mm = p->mm;
	struct memory mem;
	
	if (verbose)
		printk("Saving vm structure\n");
	
	task_lock(p);
	mem.start_code  = mm->start_code;
	mem.end_code    = mm->end_code;
	mem.start_data  = mm->start_data;
	mem.end_data    = mm->end_data;
	mem.start_brk   = mm->start_brk;
	mem.brk         = mm->brk;
	mem.start_stack = mm->start_stack;
	mem.arg_start   = mm->arg_start;
	mem.arg_end     = mm->arg_end;
	mem.env_start   = mm->env_start;
	mem.env_end     = mm->env_end;
	task_unlock(p);
	
	pack_write(file, (void*)&mem, sizeof(struct memory), 0, FROM_KERNEL);
}

static inline int should_dump(unsigned long flags)
{
	if (!(flags & VM_WRITE) || (flags & VM_MAYSHARE))
		return 0;
	else
		return 1;
}

void dump_segments(struct file *file, struct mm_struct *mm, int memleft)
{
	struct segments seg;
	struct vm_area_struct *vm = mm->mmap;
	char *buffer;
	char *line;


	if (verbose)
		printk("Saving segments\n");
	if (!(buffer = kmalloc(PAGE_SIZE, GFP_KERNEL)))
		return;
	
	for (; vm; vm = vm->vm_next) {
		seg.vm_start = vm->vm_start;
		seg.vm_end   = vm->vm_end;
		seg.prot     = vm->vm_page_prot.pgprot;
		seg.flags    = vm->vm_flags;
		seg.shared   = !should_dump(seg.flags);
		seg.pgoff    = vm->vm_file ? vm->vm_pgoff : 0;
		seg.filename[0] = 0;
		
		if (vm->vm_file) {
			struct path pth;
			
			pth.mnt    = vm->vm_file->f_vfsmnt;
			pth.dentry = vm->vm_file->f_dentry;
			line = d_path(&pth, buffer, PAGE_SIZE);
			buffer[PAGE_SIZE-1] = 0;
			ckpt_strncpy(seg.filename, line, CKPT_MAX_FILENAME);
		}
		
		pack_write(file, (void*)&seg, sizeof(struct segments), 0, FROM_KERNEL);
		
		if (memleft < sizeof(struct segments))
			memleft = PAGE_SIZE + memleft;
		memleft -= sizeof(struct segments);
	}
	
	/* 
	 * Dump the padding so the header is a mutiple of a page size, 
	 * that's what the memleft is used for.
	 */
	if (memleft > 0) {
		char *padbuf;
		padbuf = (char*)kmalloc(memleft, GFP_KERNEL);
		pack_write(file, padbuf, memleft, 0, FROM_KERNEL);
		kfree(padbuf);
	}

	kfree(buffer);
}

/*
 * Dump vm area to file.
 */
static void dump_vm_area(struct file *f, struct task_struct *p, 
			 struct vm_area_struct *vm) 
{
	char * data;
	unsigned long addr = vm->vm_start;
	static int i = 0;
	int j = 0;
		
	printk(":::Saving the vm_area for the %d time(s)", ++i);
	printk(":::%d page(s)\n", (int)(vm->vm_end - vm->vm_start + PAGE_SIZE - 1) >> 12);
	
	/* we may write to the pgtable */
	down_write(&p->mm->mmap_sem);
	
	while (addr < vm->vm_end) {
		data = get_kernel_address(p, vm, addr);
		
		if (data) {
			if ((unsigned long)data & ~PAGE_MASK)	
				printk("Warning: address %p not aligned!\n", data);
			
			if (pack_write(f, (void*)data, PAGE_SIZE, 0, FROM_KERNEL) != PAGE_SIZE)
				printk("Warning: not all dumped\n");
			j++;
		} 
		
		addr += PAGE_SIZE;
	}
	
	up_write(&p->mm->mmap_sem);
	printk("::::::%d pages saved:::\n", j);
}  



void dump_vm_areas(struct file *file, struct task_struct *p)
{
	struct vm_area_struct *vm = p->mm->mmap;
	
	if (verbose)
		printk("Saving vm areas, we have %d vm areas\n", p->mm->map_count);
	
	for (; vm; vm = vm->vm_next) {
#if 0
		printk("0x%08x - 0x%08x  ", (u32)vm->vm_start, (u32)vm->vm_end);
		print_prot( vm->vm_page_prot.pgprot);
		print_flags(vm->vm_flags);
		printk("\n");
#endif
		if (should_dump(vm->vm_flags))
			dump_vm_area(file, p, vm);		
	}
}

void dump_registers(struct file *file, struct task_struct *p, struct pt_regs *regs, int in_sys_call)
{
	if (verbose)
		printk("Saving registers\n");
	
	if (p == current) {
		/* avoid inifinte loop! */
		regs->ax = 0; 		
	} else if (in_sys_call) {
		/* If we are in a system call, we must restart it */
		regs->ip -= 2;
		regs->ax = -EINTR;
	}
	
	pack_write(file, (void *)regs, sizeof(*regs), 0, FROM_KERNEL);
}


int get_file_cnt(struct task_struct *p, struct files_struct *files)
{
	unsigned long set;
	int fds;
	int j = 0;
	int filecnt = 0;
	
	while (1) {		
		fds = j * __NFDBITS;
		if (fds >= files->fdt->max_fds)
			break;
		set = files->fdt->open_fds->fds_bits[j++];
		while (set) {
			if (set & 1) {
				if (ckpt_icheck_task(p, fds)) {
					filecnt++;
				}
			}
			
			fds++;
			set >>= 1;
		}
	}

	return filecnt;
}

int dump_open_files(struct file *file, struct task_struct *p, struct files_struct *files, int *size)
{
	struct open_files_hdr open_files_hdr;
	struct open_files open_files;
	struct ckpt_fdcache_struct *fdcache;
	char buffer[PAGE_SIZE];
	char *line;
	int fds, j = 0;
	int fdcache_size = 0;
	int file_cnt = get_file_cnt(p, files);
	unsigned long set;
	int struct_size = 0;

	if (verbose) {
		printk("Saving file table\n");
		printk("max_fds = %d\n", files->fdt->max_fds);
		printk("%d named opened files\n", file_cnt);
	}

	open_files_hdr.number_open_files = file_cnt;
	pack_write(file, (void*)&open_files_hdr, sizeof(struct open_files_hdr), 0, FROM_KERNEL);
	
	if (! (fdcache = kmalloc(sizeof(struct ckpt_fdcache_struct) * file_cnt, GFP_KERNEL)))
		return -ENOMEM;
	
	/* Now we dump them */
	for (;;) {
		fds = j * __NFDBITS;	  
		if (fds >= files->fdt->max_fds)
			break;
		set = files->fdt->open_fds->fds_bits[j++];
		while (set) {
			if (set & 1) {
				struct file *fdes = fcheck_files(files, fds);
				struct dentry *dent = fdes ? fdes->f_dentry : NULL;
				struct inode *inode = dent ? dent->d_inode : NULL;
				int i;
				
				if (!inode)
					goto next;
				
				open_files.fd = fds;
				
				// check whether this inode has appeared before
				for (i = 0; i < fdcache_size; i++) {
					if (inode == fdcache[i].inode) {
						/* cache hit */						
						if (verbose)
							printk("fd %d is a dup of fd %d\n", (int)fds, fdcache[i].fd);
						open_files.type = CKPT_DUP;		    
						open_files.u.dup.dupfd = fdcache[i].fd;
						open_files.entry_size = sizeof(struct open_files);
						pack_write(file, (void*)&open_files, sizeof(struct open_files), 0, FROM_KERNEL);		  
						struct_size += open_files.entry_size;
						goto next;
					}
				}
				
				// if not a dup, push to the cache
				fdcache[fdcache_size].fd = fds;
				fdcache[fdcache_size].inode = inode;
				fdcache_size++;
				
				if (!inode) {
					printk("fd %d has no entry\n", fds);
				} else if (S_ISSOCK(inode->i_mode)) {
					printk("fd %d is socket - unsupported\n", fds);
					return -ENOSYS;
				} else if (IS_FD_NAMED(fdes)) {         	/* *** REGULAR FILE *** */
					struct path pth;
					pth.mnt    = mntget(fdes->f_vfsmnt);
					pth.dentry = dget(dent);
					line = d_path(&pth, buffer, PAGE_SIZE);
					buffer[PAGE_SIZE-1] = 0;
					dput(pth.dentry);
					mntput(pth.mnt);
					
					open_files.type            = CKPT_FILE;
					open_files.fd              = fds;
					open_files.u.file.file     = (unsigned long)fdes;
					open_files.u.file.flags    = fdes->f_flags;
					open_files.u.file.mode     = fdes->f_mode;
					open_files.u.file.file_pos = fdes->f_pos;
					open_files.entry_size      = buffer + PAGE_SIZE - line +
						sizeof(struct open_files);
					pack_write(file, (void*)&open_files, sizeof(struct open_files), 0, FROM_KERNEL);
					
					if (verbose)
						printk("fd %02d: %04d: %08ld:  %s \n", fds, open_files.entry_size, 
						       open_files.u.file.file_pos, line);
					
					pack_write(file, (void*)line, open_files.entry_size - sizeof(struct open_files), 0, FROM_KERNEL);
					
					struct_size += open_files.entry_size;
					
				} else {
					printk("Unknown file type, cannot handle\n");
				}
			}			
		next:
			fds++;
			set >>= 1;
		}
	}
	
	*size = struct_size;
	return 0;
}


void dump_cwd(struct file *file, struct task_struct *p, int *res)
{
	struct path pth;
	char buffer[PAGE_SIZE];
	char *line;
	int size;
	
	pth.dentry = dget(p->fs->pwd.dentry);
	pth.mnt    = mntget(p->fs->pwd.mnt);
	line = d_path(&pth, buffer, PAGE_SIZE);
	buffer[PAGE_SIZE-1] = 0;
	size = buffer + PAGE_SIZE - line;	
	dput(pth.dentry);
	mntput(pth.mnt);
	
	if (verbose)
		printk("saving cwd: %s\n", line);
	pack_write(file, (void *)&size, sizeof(size), 0, FROM_KERNEL);
	pack_write(file, (void *)line, size, 0, FROM_KERNEL);

	*res = size + sizeof(size);
}

void dump_signal(struct file *file, struct task_struct *p, int *size)
{
	sigset_t blocked;
	struct signal_struct sig;
	unsigned long * signal = (unsigned long *)&p->pending.signal;
	
	if (verbose)
		printk("Saving signal handlers\n");
	
	spin_lock_irq(&p->sighand->siglock);
	
    	
	/* ignore SIGSTOP/SIGCONT */
	if ((_NSIG_WORDS == 2 && signal[0]) & ~(0x60000L || signal[1]))
		printk("pending signals not saved: %08lx %08lx\n",
		       signal[0], signal[1]);
	
	blocked = p->blocked;
	sig = *(p->signal);
	
	spin_unlock_irq(&p->sighand->siglock);
	
	pack_write(file, (void*)&blocked, sizeof(blocked), 0, FROM_KERNEL);
	pack_write(file, (void*)&sig, sizeof(sig), 0, FROM_KERNEL);
	*size = sizeof(blocked) + sizeof(sig);
}

void write_end(struct file *file)
{
	pack_write(file, NULL, 0, 1, FROM_KERNEL); /* last packet */
}
