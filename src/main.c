/*
  CRAK, Checkpoint/Restart As a Kernel module, is a Linux checkpoing/restart
  package.  It works for Linux kernel 2.2.x/i386.

  Copyright (C) 2000-2001, Hua Zhong <huaz@cs.columbia.edu>
      
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  This work originally started from Eduardo Pinheiro's epckpt Project:
  
  http://www.cs.rochester.edu/~edpin/epckpt

  but has been almost completely restructured and rewritten.

  04/22/2001:
  
  Added support for tcp/ipv4 socket (incomplete).  Using ioctl for all
  operations.
*/


#define __CHECKPOINT__

#include <linux/smp.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/binfmts.h>
#include <asm/mman.h>
#include <asm/uaccess.h>
#include <linux/file.h>
#include <linux/sys.h>
#include <asm/page.h>
#include "config.h"
#include "ckpt.h"
#include "ckptlib.h"

#include <linux/mount.h>
#include <linux/swapops.h>
#include <linux/sched.h>


/* performance testing */
//#define PRINT_TIME

#ifdef PRINT_TIME

#include <asm/msr.h>

typedef struct tcounter 
{
	unsigned long tlo;
	long thi;
} tcounter_t;

inline void gettsc(long long int* utime) 
{
	unsigned long Lo;
	long Hi;
	tcounter_t* tc = (tcounter_t*)utime;
	rdtsc(Lo,Hi);
	tc->tlo = Lo;
	tc->thi = Hi;
}

inline unsigned long hi_word(unsigned long long u) 
{
	unsigned long * ptr = (unsigned long *)&u;
	return *(ptr+1);
}

inline unsigned long lo_word(unsigned long long u) 
{
	unsigned long * ptr = (unsigned long *)&u;
	return *ptr;
}
#endif




/*
 * Whether it's inside a system call
 * the basic idea is to get the previous two chars pointed by EIP
 * to see whether they are 0xCD 0x80.
 */
static int is_sys_call(struct task_struct *p, struct pt_regs * regs) 
{
	char *ins1, *ins2;
	if (regs->orig_ax <= 0 || regs->orig_ax > NR_syscalls)
		return 0;
	
	ins1 = get_kernel_address(p, find_vma(p->mm, regs->ip - 2), regs->ip-2);
	ins2 = get_kernel_address(p, find_vma(p->mm, regs->ip - 1), regs->ip-1);
	if (ins1 && ins2 && (*ins1 == (char)0xcd) && (*ins2 == (char)0x80))
		return 1;
	else
		return 0;
}


void kstack_dump(void *top, void *end)
{
	unsigned long *p = top - 4;	
	int pro = 0;
	printk("----= ESP: %p=------", end);

	while((void *)p >= end) {
		if (((unsigned long)p & 0xf) == 0) {
			printk("\n%08x: ", pro);
			pro += 0x10;
		}
		printk("%08x ", (unsigned int)*p--);
	}
}
static inline unsigned long read_esp(void)
{
	unsigned long val;
	asm volatile("mov %%esp,%0\n\t" :"=r" (val), "=m" (__force_order));
        return val;
}



/*
 * Converts user address -> kernel address. Addr should be aligned by page.
 * Refer to handle_pte_fault() in mm/memory.c
 */
char * get_kernel_address(struct task_struct * p,
			  struct vm_area_struct *vm,
			  unsigned long addr) 
{
	pgd_t *pgd;
	pmd_t *page_middle;
	pte_t *pte;
	char *kaddr = NULL;
	int (*ckpt_handle_pte_fault)(struct mm_struct *mm,
				     struct vm_area_struct * vma,
				     unsigned long address,
				     pte_t *pte, pmd_t *pmd, 
				     int write_access) 
		= (void*)CKPT_HANDLE_PTE_FAULT;
	
	pgd = pgd_offset(p->mm, addr);	
	if (ckpt_pgd_none(*pgd)) {
		printk("none pgd: %08lx\n", (unsigned long)pgd);
		return NULL;
	}	
	if (ckpt_pgd_bad(*pgd)) {
		printk("bad pgd: %08lx\n", (unsigned long)pgd);
		pgd_ERROR(*pgd);
		return NULL;
	}
	
	page_middle = ckpt_pmd_offset(pgd, addr);
	if (pmd_none(*page_middle))
		return NULL;	
	if (pmd_bad(*page_middle)) {
		printk("Bad page middle entry %08lx\n", pmd_val(*page_middle));
		return NULL;
	}	
	
	if (!pmd_present(*page_middle)) {
		printk("Non-present page middle entry %08lx\n", pmd_val(*page_middle));
		return NULL;
	}
	
	pte = pte_offset_kernel(page_middle,addr);	
	if (pte == NULL) {
		printk("NULL pte\n");
		return NULL;
	}	
	if (!pte_present(*pte)) {
		ckpt_handle_pte_fault(p->mm, vm, addr, pte, page_middle, 
				      vm->vm_flags & VM_MAYWRITE);
		
		/* check it again */
		if (!pte_present(*pte)) {
			printk("ERROR: Page for %08lx still not present!\n", addr);
			return NULL;
		}
	}	
  	
	
	kaddr = (char *) page_address(pte_page(*pte)) + (addr & ~PAGE_MASK);
	
	return kaddr;
}


/*
 * Function to perform the act of checkpointing. Details follow.
 *
 * The order data is saved to fd is as follows:
 * -# Save the header structure.
 * -# Save the register contents.
 * -# Save the memory structure.
 * -# Save the segments structure. 
 * -# Save file descriptors.
 * -# Save the CWD.
 * -# Save signals.
 *
 * return 0 - success, else - failure.
 */
int do_checkpoint(int fd, struct task_struct * p, int flags) 
{
	struct file *f;
	struct files_struct * files = p->files;
	struct pt_regs *regs;
	mm_segment_t fs = get_fs();
	
	int memleft = PAGE_SIZE;
	int ret = 0;
	int in_sys_call;
	int offset = 0;
	int size = 0;
		
	set_fs(KERNEL_DS);
	regs = task_pt_regs(p);
#if 0
	kstack_dump((void *)task_thread_info(p) + THREAD_SIZE, read_esp());
	printk("===%08x %08x %08x %08x %p\n", regs->ss, regs->sp, regs->cs, regs->ds, (void *)regs->ip);
#endif

	task_lock(current);
	f = current->files->fdt->fd[fd]; 
	task_unlock(current);
	f->f_pos = 0;
	
	in_sys_call = is_sys_call(p, regs);
	
	/* 
	 * build the header 
	 */
	dump_header_struct(f, p, in_sys_call);
	memleft -= sizeof(struct header);
	size = sizeof(struct header);
	printk("\noffset:%08x(size: %d(0x%x))\n", offset, size, size); offset += size;
	
	/*
	 * save the registers
	 */
	printk("OFFSET: %08x\n", (u32)f->f_pos);
	dump_registers(f, p, regs, in_sys_call);
	memleft -= sizeof(struct pt_regs);
	size = sizeof(struct pt_regs);
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); 
	offset += size;
	
	/* 
	 * save the memory struct 
	 */
	dump_memory_struct(f, p);
	memleft -= sizeof(struct memory);
	size = sizeof(struct memory);
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); offset += size;
	
	/*
	 * save the segments
	 */
	dump_segments(f, p->mm, memleft);
	size = sizeof(struct segments) * p->mm->map_count;
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); 
	offset = ((offset + size + PAGE_SIZE - 1) / PAGE_SIZE) * PAGE_SIZE;
	

	
	/*
	 * save the vm areas
	 */
	set_fs(fs);
	dump_vm_areas(f, p);	
	set_fs(KERNEL_DS);
	size = PAGE_SIZE * p->mm->total_vm;
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); 
	offset += size;
	
		
	
	/*
	 * dump opened files
	 */
	ret = dump_open_files(f, p, files, &size);
	if (ret)
		return ret;
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); 
	offset += size;
	
	/* 
	 * dump CWD
	 */
	dump_cwd(f, p, &size);
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); 
	offset += size;
	
	/*
	 * dump the signal handler section
	 */
	dump_signal(f, p, &size);
	printk("offset:%08x(size: %d(0x%x))\n", offset, size, size); 
	offset += size;
	
	write_end(f);
	set_fs(fs);
	
	return ret;
}


/*
 * Checkpoint process 'pid' to a descriptor pointed by 'fd'. 
 * return :
 *  - -EBADF - Bad file descriptor.
 *  - -ESRCH - No such pid.
 *  - -EACCES - Cannot ckpt this pid.
 *
 *  - <0 - Some other error condition.
 *  - 0 - Success.
 */
static int checkpoint(int fd, pid_t pid, int flags) 
{
	struct task_struct *p;
	struct file *f;
	int ret;
	int stop = 0;
#ifdef PRINT_TIME
	long long hr_time1, hr_time2;
	gettsc(&hr_time1);
#endif	
	
	f  = fcheck(fd);
	if (!f){
		if(verbose) printk("Error: Bad fd.");
		return -EBADF;
	}
	
	p = ckpt_find_task_by_pid(pid);
	
	if (!p){
		if(verbose) printk("Error: No such pid.");
		return -ESRCH; /* no such pid */
	}
	
	if (((p->uid != current->euid && p->uid != current->uid)
	     && (current->uid && current->euid))
	    || !ckptable(p)){
		if(verbose) printk("Error: Not accessible.");
		return -EACCES;    
	} 

	// we don't checkpoint a currently running process.
	// stop it first.	
	if (p != current) {
		send_sig(SIGSTOP, p, 0);
		stop = 1;
	}
	
	if ((ret = do_checkpoint(fd, p, flags)) != 0)
		return ret;
	
	if (stop)
		send_sig(SIGCONT, p, 0);
	
	if (flags & CKPT_KILL)
		send_sig(SIGKILL, p, 0); /* kill it if possible */
	
#ifdef PRINT_TIME
	gettsc(&hr_time2);
	printk("checkpoint %s: %lu, %lu\n", p->comm, hi_word(hr_time2 - hr_time1), lo_word(hr_time2 - hr_time1));
#endif	
	
	return 0;
}



/*
 * Opens a file set to private.
 */
struct file * open_private_file(int fd, const char *filename, 
				       int flags, int mode) 
{
	return filp_open(filename, flags, mode);
}

void flush_mm_struct(struct mm_struct *mm)
{
	mm->mmap = NULL;
	mm->mm_rb.rb_node = NULL;
	mm->mmap_cache = NULL;
	mm->map_count = 0;
}

/*
 *  Restart a process that was once checkpointed.
 *  This call does not return on success. Instead, it replaces the
 *  currently running process with the checkpointed code, similar
 *  to what exec() does.
 *
 */
static int restart(int fd, const char * fname, pid_t pid, int flags) 
{
	struct file *f = NULL;
	struct header *hdr;
	struct pt_regs *regs;
	char * filename = NULL;
	int off;
	mm_segment_t fs;
	int err;
	
#ifdef PRINT_TIME
	long long hr_time1, hr_time2;
	gettsc(&hr_time1);
#endif
	
	fs = get_fs();
	
	filename = getname((char*)fname);

	err = PTR_ERR(filename);
	if (IS_ERR(filename))
		goto out;

	err = -ENOMEM;
	
	f = open_private_file(fd, filename, 0, 1);	
	if (!f || !f->f_op || !f->f_op->read) {
		printk("got a null pointer\n");
		goto out;
	}	
	f->f_pos = 0;
	
	set_fs(KERNEL_DS);

#if 0
	regs = task_pt_regs(current);
	printk("CS:IP for now:%08x:%p\n", regs->cs, (void *)regs->ip);
	printk("KernelADD for it:%p\n", 
	       get_kernel_address(current, NULL, regs->ip));
	printk("PGD for now::%p\n", current->mm->pgd);
#endif
	

	/*
	 * Restoring header structure
	 */
	if (!(hdr = restore_header_struct(f)))
		goto out;
	
	
	/*
	 * Get it first, restore it at last.
	 */
	regs = get_registers(f);



	/*
	 * Restoring memory structure
	 */
	restore_memory_struct(f, hdr);
	
	
	/* Skips the vm segments stored in the image file */
	off = (hdr->num_segments * sizeof(struct segments)) 
		+ sizeof(struct header) 
		+ sizeof(struct pt_regs) 
		+ sizeof(struct memory);
	if (off & (PAGE_SIZE - 1))
		off = (off / PAGE_SIZE + 1) * PAGE_SIZE;
	
	//flush_mm_struct(current->mm);
	/*
	 * Restore the vm areas
	 */
	off = restore_vm_areas(f, hdr->num_segments, off);
	if (off < 0) {
		err = off;
		goto out;
	}
		
	set_fs(KERNEL_DS);

	if (!f->f_op->llseek)
		f->f_pos = off;
	else
		f->f_op->llseek(f, off, 0);

	
	/*
	 * Restoring opened files 
	 */
	if (restore_open_files(f))
		goto out;

	
	/*
	 * Restoring CWD
	 */
	restore_cwd(f);	
	
	
	/* 
	 * Restoring singal  handlers
	 */
	restore_signal(f);


	err = 0;
	current->state = TASK_RUNNING;	
	if (flags & RESTART_STOP) {
		if (verbose)
			printk("Currently stopped\n");
		
		send_sig(SIGSTOP, current, 0);
	}
	
	if (verbose)
		printk("*** RESTART: done ***\n");
	
	if (hdr->in_sys_call)
		err = regs->orig_ax; // returns the number of the original syscall.
	
#if 0
	printk("CS:IP:: %p::%p SS:SP:: %p::%p\n",  (void *)regs->cs, 
	       (void *)regs->ip, (void *)regs->ss, (void *)regs->sp);
	printk("Kernel address: %p\n", get_kernel_address(current, 0,regs->ip));
	printk("PGD for now::%p\n", current->mm->pgd);
#endif
	/* 
	 * Restore the regsiter here 
	 */
	memcpy(task_pt_regs(current), regs, sizeof(*regs));
	
 out:
	if (err < 0 && verbose)
		printk("restart error: %d\n", -err);
	
	set_fs(fs);
	
	if (filename)
		putname(filename);
	
	if (flags & RESTART_NOTIFY) {
		// we need to notify someone
		
		//	  struct task_struct * p = current->p_pptr;
		struct task_struct * p = current->parent;
		// if pid == 0 notify parent
		
		if (pid > 0)
			p = ckpt_find_task_by_pid(pid);
		
		if (p) {
			if (verbose)
				printk("Notifying process %d\n", p->pid);
			
			send_sig(SIGUSR1, p, 0); // let him know!
		}
	}
#ifdef PRINT_TIME
	gettsc(&hr_time2);
	printk("restart %s: %lu, %lu\n", current->comm, hi_word(hr_time2 - hr_time1), lo_word(hr_time2 - hr_time1));
#endif	
	
	return err;		
}



/* 
 * The name for our device, as it will appear in /proc/devices 
 */
#define DEVICE_NAME "ckpt"

static int ckpt_open(struct inode *inode, struct file *file) 
{
	return 0;
}

static int ckpt_release(struct inode *inode, struct file *file) 
{
	return 0;
}

/*
 * ioctl impl. for checkpoint. Primary means to interact with device.
 */
int ckpt_ioctl(struct inode * inode_i, struct file * file,
	       unsigned int cmd, unsigned long arg) {
	struct ckpt_param param;
	
	
	if(_IOC_TYPE(cmd) != CKPT_MAGIC) 
		return -ENOTTY;
	if(_IOC_NR(cmd) > CKPT_MAX_IOC_NR) 
		return -ENOTTY;
  
	// arg is the pointer to ckpt_param
	if (copy_from_user(&param, (void *)arg, sizeof(struct ckpt_param)))
		return -EFAULT;
	
	if (verbose)
		printk("pid is %d, cmd is %d\n", param.pid, cmd);
	
	// do checkpoint/restart here
	switch(cmd){
	case CKPT_IOCTL_CHECKPOINT:
		printk("Running a checkpoint...\n");
		return checkpoint(param.fd, param.pid, param.flags);
		break;
	case CKPT_IOCTL_RESTART:
		return restart(param.fd, param.filename, param.pid, param.flags);
		break;
	default:
		return -EINVAL;    
	}
}


static int major;

struct file_operations ckpt_fops = {
 owner:		THIS_MODULE,
 ioctl:		ckpt_ioctl,
 open:		ckpt_open,
 release:	ckpt_release,
};

struct cdev ckpt_cdev;

static void setup_ckpt_cdev(dev_t devno)
{
	int err;
  
	cdev_init(&ckpt_cdev, &ckpt_fops);
	ckpt_cdev.owner = THIS_MODULE;
	ckpt_cdev.ops = &ckpt_fops;
	err = cdev_add(&ckpt_cdev, devno, 1);
	
	if(err)
		printk(KERN_WARNING "Error %d adding ckpt device", err);
}


static int ckpt_init(void) 
{
	int result;
	dev_t dev;
	
	result = alloc_chrdev_region(&dev, 0, 1, DEVICE_NAME);  
	if(result < 0){
		printk("Error allocating device ckpt w/ major %d.\n", major);
		return result;
	}

	major = MAJOR(dev);
	setup_ckpt_cdev(dev);
	
	return 0;
}

static void ckpt_cleanup(void) 
{
	cdev_del(&ckpt_cdev);
	unregister_chrdev_region(MKDEV(major,0), 1);
}

module_init(ckpt_init);
module_exit(ckpt_cleanup);

MODULE_LICENSE("GPL");
