#ifndef _CKPTLIB_H
#define _CKPTLIB_H

#if 0
#define IS_SYS_CALL(regs)	(((regs).orig_eax>=0) && 		\
				 ((regs).eax == -ERESTARTNOHAND ||	\
				  (regs).eax == -ERESTARTSYS ||		\
				  (regs).eax == -ERESTARTNOINTR) ) 
#endif

#define IS_FD_NAMED(fdes)       ((fdes)->f_dentry->d_name.name)



/* dump.c */
void dump_header_struct(struct file *, struct task_struct *, int);
void dump_registers(struct file *, struct task_struct *, struct pt_regs *, int);
void dump_memory_struct(struct file *, struct task_struct *);
void dump_segments(struct file *, struct mm_struct *, int);
void dump_vm_areas(struct file *, struct task_struct *);
int get_file_cnt(struct task_struct *, struct files_struct *);
int dump_open_files(struct file *,  struct task_struct *, 
		    struct files_struct *, int *);
void dump_cwd(struct file *, struct task_struct *, int *);
void dump_signal(struct file *, struct task_struct *, int *);

void write_end(struct file *);

/* restore.c */
struct header * restore_header_struct(struct file *);
struct pt_regs * get_registers(struct file *);
void restore_memory_struct(struct file *, struct header *);
unsigned long restore_vm_areas(struct file *, int, int);
int restore_open_files(struct file *);
void restore_cwd(struct file *);
void restore_signal(struct file *);


/* main.c */
char *get_kernel_address(struct task_struct *,
			 struct vm_area_struct *, 
			 unsigned long);
struct file *open_private_file(int, const char *, int, int);


/*
 * Extracts flags from vm_flags value.
 */
static inline unsigned long get_mmap_flags(unsigned short vm_flags) 
{
	
	return MAP_FIXED |
		(vm_flags & VM_MAYSHARE? MAP_SHARED : MAP_PRIVATE) |
		(vm_flags & VM_GROWSDOWN) |
		(vm_flags & VM_DENYWRITE) |
		(vm_flags & VM_EXECUTABLE);
}



static inline int inside(unsigned long a,
			 unsigned long b,
			 unsigned long c) 
{
	return (c <= b) && (c >= a);
}


static inline int valid_memory_segment(struct pt_regs *regs, 
				       struct mm_struct *mm, 
				       struct vm_area_struct *vma) 
{ 
	if (inside(mm->start_code, mm->end_code, vma->vm_start))
		return 1;
	else if (inside(mm->end_code, mm->end_data, vma->vm_start))
		return 1;
	else if (inside(mm->end_data, mm->brk, vma->vm_start))
		return 1;
	else if (inside(mm->start_stack, 0xC0000000, vma->vm_start))
		return 1;
	else if (inside(vma->vm_start, vma->vm_end, regs->sp))
		return 1;
	else
		return 0;
}

static char prot_char[] = {'r', 'w', 'x'};
static inline void print_prot(uint32_t prot)
{
	int i = 0;
	
	prot &= 7;
	for (; i < 3; i++) {
		if (prot & 1)
			printk("%c", prot_char[i]);
		else
			printk("-");
		
		prot >>= 1;
	}
}

static inline void print_flags(uint32_t flags)
{
	
	
	if (get_mmap_flags(flags) & MAP_SHARED)
		printk("s ");
	else
		printk("p ");

	if (flags & MAP_FIXED)
		printk("MAP_FIXED  ");
	if (flags & MAP_ANONYMOUS)
		printk("MAP_ANONYMOUS  ");
	

	if (flags & MAP_GROWSDOWN)
		printk("MAP_GROWSDOWN  ");
	if (flags & MAP_DENYWRITE)
		printk("MAP_DENYWRITE  ");
	if (flags & MAP_EXECUTABLE)
		printk("MAP_EXECUTABLE  ");
	if (flags & MAP_LOCKED)
		printk("MAP_LOCKED  ");
	if (flags & MAP_POPULATE)
		printk("MAP_POPULATE  ");
	if (flags & MAP_NONBLOCK)
		printk("MAP_NONBLOCK  ");

	printk("\n");
}

#endif
