#include <linux/smp.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/binfmts.h>
#include <asm/mman.h>
#include <asm/uaccess.h>
#include <linux/file.h>
#include <linux/sys.h>
#include <asm/page.h>
#include "config.h"
#include "ckpt.h"
#include "ckptlib.h"

#include <linux/mount.h>
#include <linux/swapops.h>
#include <linux/sched.h>


struct header * restore_header_struct(struct file *file)
{
	static struct header hdr;
	int ret;
	
	
	ret = file->f_op->read(file, (char*)&hdr, sizeof(hdr), &file->f_pos);
	
	if ((ret != sizeof(hdr)) || ckpt_strncmp(hdr.signature, "CKPT", 4) ||
	    hdr.major_version != CKPT_MAJOR ||
	    hdr.minor_version != CKPT_MINOR) {
		printk("Invalid checkpoint file\n");
		return NULL;
	}
	
	
	if (((hdr.uid != current->euid && hdr.uid != current->uid)
	     && (current->uid && current->euid))) 
		return NULL;
	
	
	/* restore uid/gid */	
	if (current->uid == 0 || current->euid == 0) {
		current->uid   = hdr.uid;
		current->euid  = hdr.euid;
		current->suid  = hdr.suid;
		current->fsuid = hdr.fsuid;
		current->gid   = hdr.gid;
		current->egid  = hdr.egid;
		current->sgid  = hdr.sgid;
		current->fsgid = hdr.fsgid;
		current->group_info->ngroups = hdr.ngroups;
	}
	
	return &hdr;
}

struct pt_regs * get_registers(struct file *file)
{
	static struct pt_regs regs;
	
	if (verbose)
		printk("Restoring registers\n");
	
	
	file->f_op->read(file, (void*)&regs, sizeof(regs), &file->f_pos);
	
	return &regs;
}

void restore_memory_struct(struct file *file, struct header *hdr)
{
	struct memory mem;
		
	if (verbose)
		printk("Reading header: %d segments\n",hdr->num_segments);

	ckpt_strncpy(current->comm, hdr->comm, 16);
	
	
	current->state = TASK_INTERRUPTIBLE;
	
	
	if (verbose)
		printk("Restoring vm structure\n");
	
	file->f_op->read(file, (void*)&mem, sizeof(struct memory), &file->f_pos);
	
	current->mm->start_code  = mem.start_code;
	current->mm->end_code    = mem.end_code;
	current->mm->start_data  = mem.start_data;
	current->mm->end_data    = mem.end_data;
	current->mm->start_brk   = mem.start_brk;
	current->mm->brk         = mem.brk;
	current->mm->start_stack = mem.start_stack;
	current->mm->arg_start   = mem.arg_start;
	current->mm->arg_end     = mem.arg_end;
	current->mm->env_start   = mem.env_start;
	current->mm->env_end     = mem.env_end;
}

unsigned long restore_vm_areas(struct file *file, int count, int from)
{
	int i;
	int err;
	unsigned long size;
	unsigned long mmap_prot, mmap_flags, ret = 0;
	struct segments seg;
	mm_segment_t fs = get_fs();
	
	if (verbose)
		printk("Restoring vm areas\n");
	
	/* Map all the segments */
	for (i = 0; i < count; i++) {
		printk("Restoring vm areas for the %d time(s)\n", i + 1);
		
		
		set_fs(KERNEL_DS);
		err = -EIO;
		file->f_op->read(file, (void*)&seg, sizeof(struct segments), &file->f_pos);
		size = seg.vm_end - seg.vm_start;		
		set_fs(fs);
		
		mmap_prot = seg.flags & 7;
		mmap_flags = get_mmap_flags(seg.flags);
		
		if (!seg.shared) {
			mmap_flags &= ~VM_EXECUTABLE;
		}
		
		if (!seg.shared) {
			int growsdown = mmap_flags & MAP_GROWSDOWN;
			
			mmap_flags &= ~ MAP_DENYWRITE;
			if (growsdown)
				mmap_flags &= ~MAP_GROWSDOWN;
			
			ret = do_mmap(file, seg.vm_start, size, mmap_prot,
				      mmap_flags | MAP_FIXED, from);
			if (growsdown)
				mmap_flags |= MAP_GROWSDOWN;

			from += size;
		} else {
#if 1
			struct file *mfile;

			print_prot(mmap_prot); print_flags(seg.flags);
			mmap_flags &= ~ MAP_DENYWRITE;
			mmap_flags |= VM_EXECUTABLE;
			mfile = open_private_file(0, seg.filename, O_RDONLY,
						  mmap_prot & PROT_WRITE ?3:1);
			if (!mfile) {
				printk("open file %s error\n", seg.filename);
				return -1;
			}
			printk("size: %ld == pgoff: %ld\n", size, seg.pgoff);
			ret = do_mmap(mfile, seg.vm_start, size, 
				      mmap_prot, mmap_flags | MAP_FIXED, 
				      seg.pgoff * PAGE_SIZE);
#else
			
			int growsdown = mmap_flags & MAP_GROWSDOWN;
			
			mmap_flags &= ~ MAP_DENYWRITE;
			if (growsdown)
				mmap_flags &= ~MAP_GROWSDOWN;
			
			ret = do_mmap(file, seg.vm_start, size, mmap_prot,
				      mmap_flags | MAP_FIXED, from);
			if (growsdown)
				mmap_flags |= MAP_GROWSDOWN;

			from += size;
#endif
		}
		
		if (ret != seg.vm_start) {
			printk("Restart: Mapping error at map #%d.",i + 1); 
			printk("Sent %lX and got %X (%ld)\n", 
			       seg.vm_start, (__u32)ret, (signed long)ret);
			
			return ret;
		} else {
			printk("file mapped successfuly at: %p\n", (void *)ret);
		}
	}

	return from;
}


int restore_open_files(struct file *file)
{
	struct open_files_hdr open_files_hdr;
	struct open_files open_files;
	int i;

	
	if (verbose)
		printk("Restoring file table\n");
	
	if (file->f_op->read(file, (void*)&open_files_hdr, sizeof(struct open_files_hdr), &file->f_pos) 
	    != sizeof(struct open_files_hdr)) {
		return 1;
	}
	
	for (i = 0; i < open_files_hdr.number_open_files; i++) {
		struct file *fdes;
		struct inode *inode;
		
		if (file->f_op->read(file, (void*)&open_files, sizeof(struct open_files), &file->f_pos) 
		    != sizeof(struct open_files)) {
			return 1;
		}
		
		fdes = current->files->fdt->fd[open_files.fd];
		inode = fdes->f_dentry->d_inode;
		
		switch (open_files.type) {
		case CKPT_DUP:
		case CKPT_FILE:	    
			/* We don't need to do anything, since 
			   someone (restart) has already dupped/opened the 
			   file. We just skip this entry.
			*/
			
			file->f_pos += open_files.entry_size - sizeof(struct open_files); 
			break;
			/*! \todo Handle the case of restarting a pipe file. Refer to do_checkpoint() 
			  for details.*/
			
			/*      
				case CKPT_PIPE:
				if ( !fdes ||
				!S_ISFIFO(inode->i_mode) ) {
				printk("WARNING: restart: fd %d was not previously open or is not a pipe!!!\n",open_files.fd);
				
				send_sig(SIGKILL, current, 0);
				goto out;
				}
				
				// Now read the information left in the pipe
				if (open_files.entry_size>0) {
				f->f_op->read(f, (void*)PIPE_BASE(*inode), open_files.entry_size, &f->f_pos);
				PIPE_LEN(*inode)+=open_files.entry_size;
				}
				break;
			*/
		case CKPT_SOCK:
			printk("Socket not supported\n");
			return 1;
		}
	}
	
	return 0;
}


void restore_signal(struct file *file)
{
	sigset_t blocked;
	struct sighand_struct sighand;
	int i;
	
	if (verbose)
		printk("Restoring signal handlers\n");
	
	file->f_op->read(file, (void*)&blocked, sizeof(sigset_t), &file->f_pos);
	file->f_op->read(file, (void*)&sighand, sizeof(sighand), &file->f_pos);
	
	spin_lock_irq(&current->sighand->siglock);	 
	
	current->blocked = blocked;
	for (i = 0; i < _NSIG; i++)
		current->sighand->action[i] = sighand.action[i];
	
	spin_unlock_irq(&current->sighand->siglock);
}


void restore_cwd(struct file *file)
{
	int size;

	/* just skip it */
	file->f_op->read(file, (void*)&size, sizeof(int), &file->f_pos);
	file->f_pos += size; 
}
